"use strict";
exports.__esModule = true;
var builder = require("botbuilder");
var restify = require("restify");
var crypto = require("crypto");
var jwt = require("jsonwebtoken");
var rp = require("request-promise");
// var request = require("request");
const request = require("request-promise");
var AchieversAuth = /** @class */ (function () {
    function AchieversAuth(server) {
        var _this = this;
        this.achieversTokenCache = {};
        server.use(restify.plugins.queryParser());
        server.get(AchieversAuth.AuthStartPath + '/:userId/:tenantId', function (req, res, next) { return _this.authStart(req, res, next); });
        server.get(AchieversAuth.AuthCallbackPath, function (req, res, next) { return _this.authCallback(req, res, next); });
        server.get(AchieversAuth.AuthStartOAuthPath, function (req, res, next) { return _this.authStartOAuth(req, res, next); });
    }
    AchieversAuth.create = function (server, connector, settings, config) {
        if (!AchieversAuth.instance) {
            AchieversAuth.instance = new AchieversAuth(server);
        }
        AchieversAuth.instance.connector = connector;
        AchieversAuth.instance.settings = settings;
        AchieversAuth.instance.config = config;
        return AchieversAuth.instance;
    };

    AchieversAuth.prototype.BotSignIn = function (session) {
        var _this = this;
        // console.log("session below");
        console.log("Inside Bot sign in..");
        // console.log(session.message.sourceEvent);
        const userId = session.message.address.user.id;
        const tenantId = session.message.sourceEvent.tenant.id;

        var issueNewCard = function () {
            var authUrl = _this.settings.appBaseUrl + AchieversAuth.AuthStartPath + '/' + userId + '/' + tenantId;
            var card = new builder.SigninCard(session).text('Sign in Achievers app').button('Sign in', authUrl);
            var msg = new builder.Message(session).attachments([card]);
            session.send(msg);
            session.endDialog();
        };

        issueNewCard();
    }

    AchieversAuth.prototype.BotSignOut = function (session,cb) {
        var _this = this;

        console.log("inside Bot sign out...");
        // console.log(session.message.sourceEvent);
        const userId = session.message.address.user.id;
        const tenantId = session.message.sourceEvent.tenant.id;
        const token = jwt.sign({
            createdDate: new Date(),
            connectorType: "msteams"
        }, _this.settings.CONNECTOR_SECRET_TOKEN)
        console.log(token);

        let options = {
            // uri: AchieversAuth.AchieversTokenUrl,
            method: 'POST',
            uri: _this.settings.appAuthURL + AchieversAuth.LogoutPath,
            body: {
                appUserId: userId,
                appId: tenantId
            },
            headers: {
                Authorization: token
            },
            json: true // Automatically parses the JSON string in the response
        };

        rp(options)
            .then(function (response) {
                console.log("response for botsignout received");
                // console.log(response);
                cb(response,null);
            })
            .catch(function (err) {
                console.log("error while executing botsignout request.")
                console.log(err);
                cb(null,err);
            })

    }

    AchieversAuth.prototype.ExtensionSignIn = function (event) {
        try {
            var _this = this;
            console.log("inside Ext sign in");
            // console.log(event);
            const userId = event.address.user.id;
            const tenantId = event.sourceEvent.tenant.id;
            console.log(userId);
            console.log(tenantId);
            var authUrl = _this.settings.appBaseUrl + AchieversAuth.AuthStartPath + '/' + userId + '/' + tenantId;
            console.log(authUrl);
            var response =
                {
                    "composeExtension": {
                        "type": "auth",
                        "suggestedActions": {
                            "actions": [
                                {
                                    "type": "openUrl",
                                    "value": authUrl,
                                    "title": "Sign in to this app"
                                }
                            ]
                        }
                    }
                };
            // if (this.achieversTokenCache[userId]) {
            //     // Use cached token  
            //     response = '';          
            // }
            // else {
            // No token cached: issue a new Signin card
            // }
            console.log("end of ExtensionSigning");

            return response;
        }
        catch (e) {
            console.log("exception in Extension Sign in.");
            console.log(e);
        }

    };

    AchieversAuth.prototype.authStart = function (req, res, next) {

        try {
            console.log("inside authStart");
            var userId = req.params.userId;
            var tenantId = req.params.tenantId;
            // console.log(this.settings);
            var authUrl = this.settings.appBaseUrl + AchieversAuth.AuthStartOAuthPath;
            var body = "\n      <html>\n        <head>\n          <script src='" + AchieversAuth.TeamsSDK + "'></script>\n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.getContext((context) => {\n              //- Save user and channel id to cookie\n              document.cookie = 'userId=' + '" + userId + "' + '; Path=/';\n               document.cookie = 'tenantId=' + '" + tenantId + "' + ';Path=/';\n               window.location = '" + authUrl + "';\n            });        \n          </script>\n        </body>\n      </html>\n    ";
            res.writeHead(200, {
                'Content-Length': Buffer.byteLength(body),
                'Content-Type': 'text/html'
            });
            res.write(body);
            res.end();
        }
        catch (e) {
            console.log("error in authstart.");
            console.log(e);
        }
    };
    AchieversAuth.prototype.authStartOAuth = function (req, res, next) {
        try {
            console.log("inside authstartOAuth");

            var cookie = this.parseCookie(req.headers.cookie);
            console.log("cookie in authStartAuth");
            console.log(cookie);

            var token = jwt.sign({
                responseUrl: '',
                appUserId: cookie.userId,
                appName: cookie.tenantId,
                appId: cookie.tenantId,
                connectorType: "msteams",
                // populate callback later
                callbackUrl: this.settings.appBaseUrl + AchieversAuth.AuthCallbackPath
            }, this.settings.CONNECTOR_SECRET_TOKEN);

            // var achieversAppId = this.settings.achieversAppId;
            // var achieversOAuthRedirectUrl = this.settings.appBaseUrl + AchieversAuth.AuthCallbackPath;
            // var achieversAppScope = this.settings.achieversAppScope;
            var achieversOAuthUrl = this.settings.appAuthURL +  AchieversAuth.AchieversTokenPath + "?state=" + token;
            res.redirect(achieversOAuthUrl, next);
        }
        catch (e) {
            console.log("Exception in authStartOAuth");
            console.log(e);
        }
    };

    AchieversAuth.prototype.authCallback = function (req, res, next) {
        try {
            console.log("inside authcallback");
            console.log("req:");
            console.log(req);
            let uId = req.query.appUserId;
            let aId = req.query.appId;
            var onAuthResultBody = function (succeeded, state) {
                return succeeded ? "\n      <html>\n        <head>\n          <script src='" + AchieversAuth.TeamsSDK + "'></script>        \n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.authentication.notifySuccess('" + state + "');\n          </script>\n        </body>\n      </html>\n      " : "\n      <html>\n        <head>\n          <script src='" + AchieversAuth.TeamsSDK + "'></script>\n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.authentication.notifyFailure();\n          </script>\n        </body>\n      </html>\n    ";
            };
            var body = '';
            var state = {
                userId: uId,
                tenantId: aId
            };

            body = onAuthResultBody(true, state);
            // console.log(AchieversAuth.TokenVal);

            res.writeHead(200, {
                'Content-Length': Buffer.byteLength(body),
                'Content-Type': 'text/html'
            });
            res.write(body);
            res.end();
        }
        catch (e) {
            console.log("Exception in inside authCallBack");
            console.log(e);
        }

    };

    AchieversAuth.prototype.getUserInfo = function (userId, tenantId,cb) {
        try {
            console.log(userId);
            console.log(tenantId);
            const token = jwt.sign({
                createdDate: new Date(),
                connectorType: "msteams"
            }, this.settings.CONNECTOR_SECRET_TOKEN)

            console.log(this.settings.CONNECTOR_SECRET_TOKEN);
            console.log(this.settings.appAuthURL);
            let options = {
                // uri: AchieversAuth.AchieversTokenUrl,
                uri: this.settings.appAuthURL + AchieversAuth.GetUserInfoPath,
                qs: {
                    appUserId: userId,
                    appId: tenantId
                },
                headers: {
                    Authorization: token
                },
                json: true // Automatically parses the JSON string in the response
            };

            rp(options)
                .then(function (response) {
                    console.log("response for getUserInfo received");
                    // console.log(response);
                    cb(response,null);
                })
                .catch(function (err) {
                    console.log("error while executing getUserInfo request.")
                    console.log(err);
                    cb(null,err);
                })
        }
        catch (e) {
            console.log("Error inside getUserInfo");
            console.log(e);
        }
        

        var response = {
            user:{
                achieverAccessToken: 'fC18Z8ECjwQ8t4/eKrwM9/xf/LZzvsxQmTBQ2GEeN9aZ/CAm9p/qqIQezvza+WsvNNYRs3Ly/MGLCT9RjIwEd+77SdRAvRAyYe7Mc0lJGRYGw2Q='
            }
        };
        cb(response,null);
    }    

    AchieversAuth.prototype.parseCookie = function (rawCookie) {
        var cookies = {};
        if (rawCookie) {
            var c = rawCookie.split('; ');
            for (var i = c.length - 1; i >= 0; i--) {
                var v = c[i].split('=');
                cookies[v[0]] = v[1];
            }
        }
        return cookies;
    };

    AchieversAuth.prototype.encryptState = function (state) {
        var cipher = crypto.createCipher('aes192', this.settings.achieversAppClientSecret);
        var encryptedState = cipher.update(JSON.stringify(state), 'utf8', 'base64');
        encryptedState += cipher.final('base64');
        return encryptedState;
    };
    AchieversAuth.prototype.decryptState = function (rawState) {
        var decipher = crypto.createDecipher('aes192', this.settings.achieversAppClientSecret);
        var state = decipher.update(rawState, 'base64', 'utf8');
        state += decipher.final('utf8');
        var parsedState = JSON.parse(state);
        return parsedState;
    };

    AchieversAuth.AuthStartPath = "/auth/start";
    AchieversAuth.AuthStartOAuthPath = "/auth/oauth";
    AchieversAuth.AuthCallbackPath = "/auth/callback";
    AchieversAuth.AchieversTokenPath = '';
    AchieversAuth.GetUserInfoPath = '/api/shared/userInfo';
    AchieversAuth.LogoutPath = '/api/shared/logout';
    AchieversAuth.TeamsSDK = 'https://statics.teams.microsoft.com/sdk/v1.4.2/js/MicrosoftTeams.js';
    return AchieversAuth;
}());
exports.AchieversAuth = AchieversAuth;
