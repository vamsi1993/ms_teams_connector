const restify = require('restify');
const botbuilder = require('botbuilder');

var adapter = new botbuilder.BotFrameworkAdapter({
    appId: process.env.appId,
    appPassword: process.env.appPassword
});

//create Http server
let server = restify.createServer();

server.listen(3978,function(){
    console.log(`\n${server.name} listening to ${server.url}`);
    console.log(`\nGet Bot Framework Emulator: https://aka.ms/botframework-emulator`);
});

server.post('/api/messages',(req,res) => {
    adapter.processActivity(req,res,async(turncontext) => {
        if(turncontext.activity.type === 'message'){
            const utterance = turncontext.activity.text;

            await turncontext.sendActivity(`I heard you say ${ utterance }`);
        }
    })    
});
