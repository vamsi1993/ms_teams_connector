"use strict";
exports.__esModule = true;
var builder = require("botbuilder");
var restify = require("restify");
var crypto = require("crypto");
// var request = require("request");
const request = require("request-promise");
var AchieversAuth = /** @class */ (function () {
    function AchieversAuth(server) {
        var _this = this;
        this.achieversTokenCache = {};
        server.use(restify.plugins.queryParser());
        server.get(AchieversAuth.AuthStartPath + '/:userId', function (req, res, next) { return _this.authStart(req, res, next); });
        server.get(AchieversAuth.AuthCallbackPath, function (req, res, next) { return _this.authCallback(req, res, next); });
        server.get(AchieversAuth.AuthStartOAuthPath, function (req, res, next) { return _this.authStartOAuth(req, res, next); });
    }
    AchieversAuth.create = function (server, connector, settings, config) {
        if (!AchieversAuth.instance) {
            AchieversAuth.instance = new AchieversAuth(server);
        }
        AchieversAuth.instance.connector = connector;
        AchieversAuth.instance.settings = settings;
        AchieversAuth.instance.config = config;
        return AchieversAuth.instance;
    };

    AchieversAuth.prototype.BotSignIn = function (session){
        var _this = this;
        // console.log("session below");
        // console.log(session);
        console.log("Inside Bot sign in..");
        const userId = session.message.address.user.id;

        var authUrl = _this.settings.appBaseUrl + AchieversAuth.AuthStartPath + '/' + userId;

        var issueNewCard = function () {
            var authUrl = _this.settings.appBaseUrl + AchieversAuth.AuthStartPath + '/' + userId;
            var card = new builder.SigninCard(session).text('Sign in Achievers app').button('Login', authUrl);
            var msg = new builder.Message(session).attachments([card]);
            session.send(msg);
            session.endDialog();
        };

        issueNewCard();
    }

    AchieversAuth.prototype.ExtensionSignIn = function (event) {
        var _this = this;
        const userId = event.address.user.id;
        // var issueNewCard = function () {
        //     var authUrl = _this.settings.baseUrl + SimpleFBAuth.AuthStartPath + '/' + userId;
        //     var card = new builder.SigninCard(session).text('Sign in Achievers app').button('Login', authUrl);
        //     var msg = new builder.Message(session).attachments([card]);
        //     session.send(msg);
        //     session.endDialog();
        // };
        var authUrl = _this.settings.appBaseUrl + AchieversAuth.AuthStartPath + '/' + userId;
        var response =
            {
                "composeExtension": {
                    "type": "auth",
                    "suggestedActions": {
                        "actions": [
                            {
                                "type": "openUrl",
                                "value": authUrl,
                                "title": "Sign in to this app"
                            }
                        ]
                    }
                }
            };
        // if (this.achieversTokenCache[userId]) {
        //     // Use cached token  
        //     response = '';          
        // }
        // else {
            // No token cached: issue a new Signin card
        // }
        console.log("end of ExtensionSigning");
        
        return response;
    };

    AchieversAuth.prototype.authStart = function (req, res, next) {
        console.log("inside authStart");
        var userId = req.params.userId;
        // console.log(this.settings);
        var authUrl = this.settings.appBaseUrl + AchieversAuth.AuthStartOAuthPath;
        var body = "\n      <html>\n        <head>\n          <script src='" + AchieversAuth.TeamsSDK + "'></script>\n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.getContext((context) => {\n              //- Save user and channel id to cookie\n              document.cookie = 'userId=' + '" + userId + "' + '; Path=/';\n              if (context.channelId) {\n                document.cookie = 'channelId=' + context.channelId + ';Path=/';\n              } else {\n                document.cookie = 'channelId=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';\n              }\n              window.location = '" + authUrl + "';\n            });        \n          </script>\n        </body>\n      </html>\n    ";
        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(body),
            'Content-Type': 'text/html'
        });
        res.write(body);
        res.end();
    };
    AchieversAuth.prototype.authStartOAuth = function (req, res, next) {
        console.log("inside authstartOAuth");
        var achieversAppId = this.settings.achieversAppId;
        var achieversOAuthRedirectUrl = this.settings.appBaseUrl + AchieversAuth.AuthCallbackPath;
        var achieversAppScope = this.settings.achieversAppScope;
        var achieversOAuthUrl = "https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/authorize?response_type=code&client_id=" + this.settings.achieversAppClientId + "&scope=" + this.settings.achieversAppScope + "&state=" + "xyz";
        res.redirect(achieversOAuthUrl, next);
    };

    AchieversAuth.prototype.authCallback = function (req, res, next) {
        console.log("inside authcallback");
        AchieversAuth.AccessCode = req.query.code;
        var onAuthResultBody = function (succeeded, state) {
            return succeeded ? "\n      <html>\n        <head>\n          <script src='" + AchieversAuth.TeamsSDK + "'></script>        \n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.authentication.notifySuccess('" + state + "');\n          </script>\n        </body>\n      </html>\n      " : "\n      <html>\n        <head>\n          <script src='" + AchieversAuth.TeamsSDK + "'></script>\n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.authentication.notifyFailure();\n          </script>\n        </body>\n      </html>\n    ";
        };
        var body = '';
        if (req.query.code) {
            console.log("parsing cookie");
            var cookie = this.parseCookie(req.headers.cookie);
            var state = {
                userId: cookie.userId,
                accessCode: AchieversAuth.AccessCode,
                channelId: cookie.channelId
            };
            
            body = onAuthResultBody(true, this.encryptState(state));
        }
        else {
            body = onAuthResultBody(false);
        }

        let tokenPostObj = {
            "grant_type": "authorization_code",
            "code":  AchieversAuth.AccessCode,
            "client_id": this.settings.achieversAppClientId,
            "client_secret": this.settings.achieversAppClientSecret
        };

        const options = {
            method: 'POST',
            uri: AchieversAuth.AchieversTokenUrl,
            body: tokenPostObj,
            json: true
            // JSON stringifies the body automatically
        };

        request(options)
                .then(function(response){
                    console.log("response from achievers token retrieval: ");
                    // console.log(response);
                    console.log(response.access_token);
                    AchieversAuth.TokenVal = response.access_token;                    
                })
                .catch(function(err){
                    console.log("error in token retrieval. error:");
                    console.log(err);
                });
        // console.log("Token Val below:");
        // console.log(AchieversAuth.TokenVal);
        
        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(body),
            'Content-Type': 'text/html'
        });
        res.write(body);
        res.end();
    };

    AchieversAuth.prototype.parseCookie = function (rawCookie) {
        var cookies = {};
        if (rawCookie) {
            var c = rawCookie.split('; ');
            for (var i = c.length - 1; i >= 0; i--) {
                var v = c[i].split('=');
                cookies[v[0]] = v[1];
            }
        }
        return cookies;
    };

    AchieversAuth.prototype.encryptState = function (state) {
        var cipher = crypto.createCipher('aes192', this.settings.achieversAppClientSecret);
        var encryptedState = cipher.update(JSON.stringify(state), 'utf8', 'base64');
        encryptedState += cipher.final('base64');
        return encryptedState;
    };
    AchieversAuth.prototype.decryptState = function (rawState) {
        var decipher = crypto.createDecipher('aes192', this.settings.achieversAppClientSecret);
        var state = decipher.update(rawState, 'base64', 'utf8');
        state += decipher.final('utf8');
        var parsedState = JSON.parse(state);
        return parsedState;
    };

    AchieversAuth.AuthStartPath = "/auth/start";
    AchieversAuth.AuthStartOAuthPath = "/auth/oauth";
    AchieversAuth.AuthCallbackPath = "/auth/callback";
    AchieversAuth.TeamsSDK = 'https://statics.teams.microsoft.com/sdk/v1.0/js/MicrosoftTeams.min.js';
    AchieversAuth.TokenVal = '';
    AchieversAuth.AccessCode = '';
    AchieversAuth.AchieversTokenUrl = 'https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/token';
    return AchieversAuth;
}());
exports.AchieversAuth = AchieversAuth;
