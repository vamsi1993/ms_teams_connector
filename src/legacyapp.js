const express = require("express");
const builder = require("botbuilder");
const teams = require("botbuilder-teams");
const restify = require("restify");
const mysql = require("mysql");
const crypto = require("crypto");
const request = require("request-promise");
const curl = require("curl-request");
const http = require("http");

const PORT = process.env.PORT || 3978;
const MicrosoftAppId = '993c5cf4-d8ae-4cc5-94b3-b8af0505205f';
const MicrosoftAppPassword = '0HDlAWE]E8U_T0daYo_EOeV3Wr.1UW_v';
const baseUrl = 'https://achievers-bot-trial.herokuapp.com';
const TeamsSDK = 'https://statics.teams.microsoft.com/sdk/v1.4.2/js/MicrosoftTeams.js';
const achieversTokenUrl = 'https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/token';

// const dbHost = 'localhost';
// const dbUser = 'root';
// const dbPassword = 'root';

const achieversClientId = 'apiexa_1df764bcf06e2ac24e06dd843beb5d9547532bb73b4eebb017e044515f2e4423';
const achieversClientSecret = 'ce23c19c69c353fbcd793cd2ac664bbd2a5ef6a8931d592278011b792f7bf765';
const achieversScope = 'read write';
let userAuthenticated = false;
let authCode = '';
let authToken = '';

let tokenContainer = [];

const connector = new teams.TeamsChatConnector({
    appId: MicrosoftAppId,
    appPassword: MicrosoftAppPassword
});

var botSigninSettings = {
    appBaseUrl: baseUrl,
    achieversAppClientId: achieversClientId,
    achieversAppClientSecret: achieversClientSecret,
    achieversAppScope: achieversScope // put Achievers access scope
};

const server = restify.createServer();

server.listen(PORT, function () {
    console.log(`listening to ${PORT}`);
});

// const botAuth = achieversAuth.SimpleFBAuth.create(server, connector, botSigninSettings);

var inMemoryStorage = new builder.MemoryBotStorage();

// connector.setAllowedTenants([process.env.TenantId]);

server.get('/', function (req, res) {
    console.log("*************** inside get **********************");
    res.json({ "message": "Success" });
});

server.post('/api/messages', connector.listen());

server.use(restify.plugins.queryParser());

server.get('/docs/*',restify.plugins.serveStatic({
    directory: './pages/',
    default: 'AchieversRecognitionUI.html',
    appendRequestPath: false
}));

server.get('/api/auth', function (req, res, next) {

    callback = function (req, res, next) {
        console.log('Call back has been hit');
        // console.log(req);
        var onAuthResultBody = function (succeeded, state) {
            console.log("initiating return funtion");
            // return succeeded ? "\n      <html>\n        <head>\n          <script src='" + TeamsSDK + "'></script>        \n        </head>\n        <body>\n          <script>\n           window.onload = function(){ console.log('inside onload'); microsoftTeams.initialize();\n            microsoftTeams.authentication.notifySuccess('" + state + "'); }\n          </script>\n        </body>\n      </html>\n      " : "\n      <html>\n        <head>\n          <script src='" + TeamsSDK + "'></script>\n        </head>\n        <body>\n          <script>\n         window.onload = function(){ console.log('inside onload'); microsoftTeams.initialize();\n            microsoftTeams.authentication.notifyFailure();}\n          </script>\n        </body>\n      </html>\n    ";
            return succeeded ? "\n      <html>\n        <head>\n          <script src='" + TeamsSDK + "'></script>        \n        </head>\n        <body>\n          <script>\n           window.onload = function(){ console.log('inside onload'); microsoftTeams.initialize();\n            microsoftTeams.authentication.notifySuccess('" + state + "');  open(location, '_self').close();}\n          </script>\n        </body>\n      </html>\n      " : "\n      <html>\n        <head>\n          <script src='" + TeamsSDK + "'></script>\n        </head>\n        <body>\n          <script>\n         window.onload = function(){ console.log('inside onload'); microsoftTeams.initialize();\n            microsoftTeams.authentication.notifyFailure(); open(location, '_self').close();}\n          </script>\n        </body>\n      </html>\n    ";
        };
        var body = '';
        // console.log("request: ");
        // console.log(req);
        if (req.query.code) {
            //console.log("inside if req_query_code")
            // var cookie = this.parseCookie(req.headers.cookie);
            // console.log("header:");
            // console.log(req.headers);
            // var state = {
            //     userId: cookie.userId,
            //     accessCode: req.query.code,
            //     channelId: cookie.channelId
            // };
            // console.log("state:");
            //console.log(state);
            state = req.query.code;
            authCode = req.query.code;
            console.log(authCode);
            console.log(achieversClientId);
            let tokenPostObj = {
                "grant_type": "authorization_code",
                "code":  authCode,
                "client_id": achieversClientId,
                "client_secret": achieversClientSecret
            };

            
            const options = {
                method: 'POST',
                uri: achieversTokenUrl,
                body: tokenPostObj,
                json: true
                // JSON stringifies the body automatically
            };

            let tokenResponse = request(options)
                .then(function(response){
                    console.log("response from achievers token retrieval: ");
                    // console.log(response);
                    console.log(response.access_token);
                    authToken = response.access_token;
                    return response;
                })
                .catch(function(err){
                    console.log("error in token retrieval. error:");
                    console.log(err);
                });
            
            // console.log("tokenResponse");
            // console.log(tokenResponse);

            body = onAuthResultBody(true, this.encryptState(state));
        }
        else {
            body = onAuthResultBody(false);
        }
        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(body),
            'Content-Type': 'text/html'
        });
        res.write(body);
        res.end();
    }

    return this.callback(req, res, next);
});

const bot = new builder.UniversalBot(connector).set('storage', inMemoryStorage);

bot.dialog('/', [
    function (session) {
        var teamId = session.message.sourceEvent.team.id;
        console.log(teamId);
        // session.endDialog("Hi. Your Team Id: " + teamId);
        connector.fetchMembers(session.message.address.serviceUrl, teamId, function (err, result) {
            if (err) {
                session.endDialog('There is some error');
            }
            else {
                session.endDialog('%s', JSON.stringify(result));
            }
        })
        session.send('You Said "' + session.message.text + '"');

        session.endDialog();
    }
]);
bot.dialog('FetchChannelList', function (session) {
    // session.endDialog("Hello There");
});
bot.on('conversationUpdate', function (message) {
    // console.log(message);
});



const checkOrgAuth = function (teamId) {
    return false;
}
// const checkUserAuth = function()

const composeExtensionSubmitActionHandler = function (value) {
    //Submit event handling comes here.
};

const composeExtensionFetchHandler = function (event, req, callback) {
    // console.log(req);  
    console.log(event);

    let userId;
    let conversationId;
    let serviceUrl;
    let email;
    let tenantId;

    userId = event.address.user.id;
    conversationId = event.address.conversation.id;
    serviceUrl = event.address.serviceUrl;
    console.log("ConversationId below");
    console.log(conversationId);

    connector.fetchMembers(serviceUrl,conversationId,function(err,result){
        if (err) {
            console.log('Fetch members error');
            console.log(err);
        }
        else {
            email = result.email;
            tenantId = result.tenantId;            
        }
    });

    if (authToken == undefined | !authToken | authCode == '' ) {
        var response =
            {
                "composeExtension": {
                    "type": "auth",
                    "suggestedActions": {
                        "actions": [
                            {
                                "type": "openUrl",
                                "value": "https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/authorize?response_type=code&client_id=" + achieversClientId + "&scope=" + achieversScope + "&state=" + "xyz",
                                "title": "Sign in to this app"
                            }
                        ]
                    }
                }
            };
        // var response =
        // {
        //     "task": {
        //         "type": "continue",
        //         "value": {
        //             "card": {
        //                 "contentType": "application/vnd.microsoft.card.adaptive",
        //                 "content": {
        //                     "body": [
        //                         {
        //                             "type": "TextBlock",
        //                             "size": "Medium",
        //                             "weight": "Bolder",
        //                             "text": "Please close this window after logging in and click again on the extension to continue."
        //                         }],
        //                     "actions": [                                
        //                         {
        //                             "type": "Action.OpenUrl",
        //                             "title": "sign in",
        //                             "url": "https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/authorize?response_type=code&client_id=" + achieversClientId + "&scope=" + achieversScope + "&state=" + "xyz"
        //                         }
        //                     ],
        //                     "type": "AdaptiveCard",
        //                     "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        //                     "version": "1.0"
        //                 }
        //             }
        //         }
        //     }
        // };
        // var response = {
        //     "task": {
        //         "value": {
        //             "url": "https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/authorize?response_type=code&client_id=" + achieversClientId + "&scope=" + achieversScope + "&state=" + "xyz"
        //         },
        //         "type": "auth"
        //     }
        // };
        callback(null, response, 200);
    }
    else if (req.commandId == "OpenForm") {
        console.log("Inside OPenFOrm");
        console.log("Auth token below: ");
        console.log(authToken);
        var response = new teams.TaskModuleContinueResponse()
            .url(baseUrl + "docs/AchieversRecognitionUI.html")
            .height("large")
            .width("large")
            .fallbackUrl("https://example.sandbox.achievers.com/")
            .title("Achievers Recognition portal");

        console.log("response object created");
        // console.log(response.toResponseOfFetch());
        callback(null, response.toResponseOfFetch(), 200);
    }
};

connector.onComposeExtensionSubmitAction(composeExtensionSubmitActionHandler);

connector.onComposeExtensionFetchTask(composeExtensionFetchHandler)

var composeExtensionHandler = function (event, query, callback) {
    //parameters should be identical to manifest
    //console.log(query);
    // if(query.parameters[0].name == "initialRun")
    // {
    //     var logo = {
    //         alt: "logo",
    //         url: "http://logo.jpg",
    //         tap: null
    //     };
    //     try {
    //         console.log("inside try");
    //         var card = new builder.ThumbnailCard()
    //             .title("sample title")
    //             .images([logo])
    //             .text("sample text")
    //             .buttons([
    //             {
    //                 type: "openUrl",
    //                 title: "Go to somewhere",
    //                 value: "https://url.com"
    //             }
    //         ]);
    //         console.log("reached response");
    //         var response = teams.ComposeExtensionResponse.result("list").attachments([card.toAttachment()]);
    //         callback(null, response.toResponse(), 200);
    //     }
    //     catch (e) {
    //         callback(e, null, 500);
    //     }
    // }
    if (authToken == undefined | !authToken | authCode == '' ) {
        var response =
        {
            "composeExtension": {
                "type": "auth",
                "suggestedActions": {
                    "actions": [
                        {
                            "type": "openUrl",
                            "value": "https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/authorize?response_type=code&client_id=" + achieversClientId + "&scope=" + achieversScope + "&state=" + "xyz",
                            "title": "Sign in to this app"
                        }
                    ]
                }
            }
        };
    }
    else
    {
        var response =
        {
            "task": {
                "type": "continue",
                "value": {
                    "card": {
                        "contentType": "application/vnd.microsoft.card.adaptive",
                        "content": {
                            "body": [
                                {
                                    "type": "TextBlock",
                                    "size": "Medium",
                                    "weight": "Bolder",
                                    "text": "Please close this window after logging in and click again on the extension to continue."
                                }],
                            "actions": [                                
                                {
                                    "type": "Action.OpenUrl",
                                    "title": "sign in",
                                    "url": "https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/authorize?response_type=code&client_id=" + achieversClientId + "&scope=" + achieversScope + "&state=" + "xyz"
                                }
                            ],
                            "type": "AdaptiveCard",
                            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                            "version": "1.0"
                        }
                    }
                }
            }
        };
    }
    
    callback(null, response, 200);

};
connector.onQuery('sample', composeExtensionHandler);

parseCookie = function (rawCookie) {
    var cookies = {};
    if (rawCookie) {
        var c = rawCookie.split('; ');
        for (var i = c.length - 1; i >= 0; i--) {
            var v = c[i].split('=');
            cookies[v[0]] = v[1];
        }
    }
    return cookies;
};

encryptState = function (state) {
    var cipher = crypto.createCipher('aes192', achieversClientSecret);
    var encryptedState = cipher.update(JSON.stringify(state), 'utf8', 'base64');
    encryptedState += cipher.final('base64');
    return encryptedState;
};
decryptState = function (rawState) {
    var decipher = crypto.createDecipher('aes192', achieversClientSecret);
    var state = decipher.update(rawState, 'base64', 'utf8');
    state += decipher.final('utf8');
    var parsedState = JSON.parse(state);
    return parsedState;
};

getUserToken = function (_userId) {
    if (!tokenContainer) {
        return undefined;
    }
    else {
        for (let i = 0; i < tokenContainer.length; i++) {
            if (tokenContainer[i].userId === _userId) {
                return tokenContainer[i].token;
            }
        }
        return undefined;
    }
};

// connector.onSigninStateVerification(function (event, query, callback) { return botAuth.verifySigninState(event, query, callback); });

// connector.onComposeExtensionSubmitAction
// bot.dialog('Signin', function (session) { return botAuth.botSignIn(session); });
// bot.dialog('Signout', function (session) { return botAuth.botSignOut(session); });
// connector.onSigninStateVerification(function (event, query, callback) { return botAuth.verifySigninState(event, query, callback); });