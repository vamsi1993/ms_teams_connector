const express = require("express");
const builder = require("botbuilder");
const teams = require("botbuilder-teams");
const restify = require("restify");
const mysql = require("mysql");
const crypto = require("crypto");
const request = require("request-promise");
const curl = require("curl-request");
const http = require("http");
const achAuth = require("./achieversauth");

const PORT = process.env.PORT || 3978;
const MicrosoftAppId = '993c5cf4-d8ae-4cc5-94b3-b8af0505205f';
const MicrosoftAppPassword = '0HDlAWE]E8U_T0daYo_EOeV3Wr.1UW_v';
const baseUrl = 'https://achievers-bot-trial.herokuapp.com';
const TeamsSDK = 'https://statics.teams.microsoft.com/sdk/v1.4.2/js/MicrosoftTeams.js';
const achieversTokenUrl = 'https://example.sandbox.achievers.com/oauth/v2/openIDConnectClient/token';

// const dbHost = 'localhost';
// const dbUser = 'root';
// const dbPassword = 'root';

const achieversClientId = 'apiexa_1df764bcf06e2ac24e06dd843beb5d9547532bb73b4eebb017e044515f2e4423';
const achieversClientSecret = 'ce23c19c69c353fbcd793cd2ac664bbd2a5ef6a8931d592278011b792f7bf765';
const achieversScope = 'read write';
let userAuthenticated = false;
let authCode = '';
let authToken = '';

let tokenContainer = [];

const connector = new teams.TeamsChatConnector({
    appId: MicrosoftAppId,
    appPassword: MicrosoftAppPassword
});

var botSigninSettings = {
    appBaseUrl: baseUrl,
    achieversAppClientId: achieversClientId,
    achieversAppClientSecret: achieversClientSecret,
    achieversAppScope: achieversScope // put Achievers access scope
};

const server = restify.createServer();

server.listen(PORT, function () {
    console.log(`listening to ${PORT}`);
});

var botAuth = achAuth.AchieversAuth.create(server, connector, botSigninSettings);
// const botAuth = achieversAuth.SimpleFBAuth.create(server, connector, botSigninSettings);

var inMemoryStorage = new builder.MemoryBotStorage();

// connector.setAllowedTenants([process.env.TenantId]);

server.get('/', function (req, res) {
    console.log("*************** inside get **********************");
    res.json({ "message": "Success" });
});

server.post('/api/messages', connector.listen());

server.use(restify.plugins.queryParser());

server.get('/docs/*', restify.plugins.serveStatic({
    directory: './pages/',
    default: 'AchieversRecognitionUI.html',
    appendRequestPath: false
}));

const bot = new builder.UniversalBot(connector).set('storage', inMemoryStorage);

bot.dialog('/', [
    function (session) {
        var teamId = session.message.sourceEvent.team.id;
        console.log(teamId);
        // session.endDialog("Hi. Your Team Id: " + teamId);
        connector.fetchMembers(session.message.address.serviceUrl, teamId, function (err, result) {
            if (err) {
                session.endDialog('There is some error');
            }
            else {
                session.endDialog('%s', JSON.stringify(result));
            }
        })


        session.send('You Said "' + session.message.text + '"');

        session.endDialog();
    }
]);
bot.dialog('FetchChannelList', function (session) {
    // session.endDialog("Hello There");
});
bot.on('conversationUpdate', function (message) {
    // console.log(message);
});



const checkOrgAuth = function (teamId) {
    return false;
}
// const checkUserAuth = function()

const composeExtensionSubmitActionHandler = function (value) {
    //Submit event handling comes here.
};



const composeExtensionFetchHandler = function (event, req, callback) {
    // console.log(req);  
    console.log(event);

    let userId;
    let conversationId;
    let serviceUrl;
    let email;
    let tenantId;

    userId = event.address.user.id;
    conversationId = event.address.conversation.id;
    serviceUrl = event.address.serviceUrl;
    console.log("ConversationId below");
    console.log(conversationId);

    connector.fetchMembers(serviceUrl, conversationId, function (err, result) {
        if (err) {
            console.log('Fetch members error');
            console.log(err);
        }
        else {
            email = result.email;
            tenantId = result.tenantId;
        }
    });

    // console.log("Inside OPenFOrm");
    // console.log("Auth token below: ");
    // console.log(authToken);
    var response = new teams.TaskModuleContinueResponse()
        .url(baseUrl + "docs/AchieversRecognitionUI.html")
        .height("large")
        .width("large")
        .fallbackUrl("https://example.sandbox.achievers.com/")
        .title("Achievers Recognition portal");

    console.log("response object created");
    // console.log(response.toResponseOfFetch());
    callback(null, response.toResponseOfFetch(), 200);
};

connector.onComposeExtensionSubmitAction(composeExtensionSubmitActionHandler);

connector.onComposeExtensionFetchTask(composeExtensionFetchHandler);

connector.onInvoke(invokeHandler);

var invokeHandler = function(event,callback){
    console.log("inside invokeHandler");
    let invokeType = event.name;
    let invokeValue = event.value;
    if (invokeType === undefined) {
        invokeType = null;
    }
    switch(invokeType)
    {
        case "task/fetch":{
            var response = new teams.TaskModuleContinueResponse()
            .url(baseUrl + "docs/AchieversRecognitionUI.html")
            .height("large")
            .width("large")
            .fallbackUrl("https://example.sandbox.achievers.com/")
            .title("Achievers Recognition portal");

            callback(null, response.toResponseOfFetch(),200);
        }
    }
}

var composeExtensionHandler = function (event, query, callback) {
    console.log("inside composeExtensionHandler");
    if (achAuth.AchieversAuth.TokenVal.length > 0) {
        // resp = {
        //     "name": "composeExtension/submitAction",
        //     "type": "invoke",
        //     "value": {
        //         "type": "task/fetch"
        //     }
        // };
        console.log("inside auth complete if condition");
        resp = 
        {
            "task": {
                "type": "continue",
                "value": {
                    "card": {
                        "contentType": "application/vnd.microsoft.card.adaptive",
                        "content": {
                            "actions": [                                
                                {
                                    "type": "invoke",
                                    "title": "Recognize",
                                    "value": {
                                        "type": "task/fetch",
                                    }
                                }
                            ],
                            "type": "AdaptiveCard",
                            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                            "version": "1.0"
                        }
                    }
                }
            }
        };
        
        callback(null, resp, 200);
    }
    else {
        let resp = botAuth.ExtensionSignIn(event);
        console.log(resp);
        callback(null, resp, 200);
    }
};
connector.onQuery('sample', composeExtensionHandler);

connector.onSigninStateVerification(function (event, query, callback) {
    console.log("inside Sign in verification");
    return botAuth.verifySigninState(event, query, callback);
});

// connector.onSigninStateVerification(function (event, query, callback) { return botAuth.verifySigninState(event, query, callback); });

// connector.onComposeExtensionSubmitAction
// bot.dialog('Signin', function (session) { return botAuth.botSignIn(session); });
// bot.dialog('Signout', function (session) { return botAuth.botSignOut(session); });
// connector.onSigninStateVerification(function (event, query, callback) { return botAuth.verifySigninState(event, query, callback); });