const express = require("express");
const builder = require("botbuilder");
const teams = require("botbuilder-teams");
const restify = require("restify");
const mysql = require("mysql");
const crypto = require("crypto");
const request = require("request-promise");
const curl = require("curl-request");
const achAuth = require("./middlewareAuth.js");
const render = require('restify-render-middleware');
const dotenv = require('dotenv');

const config = dotenv.config();

const PORT = process.env.PORT || 4001;
// const MicrosoftAppId = '993c5cf4-d8ae-4cc5-94b3-b8af0505205f';
// const MicrosoftAppPassword = '0HDlAWE]E8U_T0daYo_EOeV3Wr.1UW_v';
// const baseUrl = 'https://achievers-bot-trial.herokuapp.com';
// const authURL = 'https://connect.devapi.achievers.com';
// const connectorSecretToken = 'asl@Wb83$dbid$bFd8$#KB$DH$ea';
// const achieversBaseUrl = 'https://example.sandbox.achievers.com/api/v5';

const MicrosoftAppId = process.env.APP_ID;
const MicrosoftAppPassword = process.env.APP_SECRET;
const baseUrl = process.env.BASE_URL;
const authURL = process.env.AUTH_URL;
const connectorSecretToken = process.env.CONNECTOR_SECRET_TOKEN;
const achieversBaseUrl = process.env.ACHIEVERS_BASE_URL;

console.log(MicrosoftAppId);
console.log(MicrosoftAppPassword);

const connector = new teams.TeamsChatConnector({
    appId: MicrosoftAppId,
    appPassword: MicrosoftAppPassword
});

console.log(`Auth URL: ${authURL}`);

var botSigninSettings = {
    appBaseUrl: baseUrl,
    appAuthURL: authURL,
    CONNECTOR_SECRET_TOKEN: connectorSecretToken
};

console.log(`botSigninSettings: ${botSigninSettings.appAuthURL}`);

var viewSettings = {
    userFullName: '',
    userName: '',
    userId: '',
    tenantId: '',
    userEmail: '',
    baseAppUrl: baseUrl,
    getNames: "/api/achievers/userFetch",
    getRecogTypes: "/api/achievers/getRecogTypes",
    postrecog: "/api/achievers/postRecog",
    appId: MicrosoftAppId
};

const achieverAPI = {
    baseUrl: achieversBaseUrl,
    getUsers: '/users',
    getRecogTypes: '/recognitions',
    postRecog: '/recognitions'
};


const server = restify.createServer();

server.listen(PORT, function () {
    console.log(`listening to ${PORT}`);
});

var botAuth = achAuth.AchieversAuth.create(server, connector, botSigninSettings, config);
// const botAuth = achieversAuth.SimpleFBAuth.create(server, connector, botSigninSettings);

var inMemoryStorage = new builder.MemoryBotStorage();
var name = '';

//connector.setAllowedTenants([process.env.TenantId]);

server.get('/', function (req, res) {
    console.log("*************** inside get **********************");
    res.json({ "message": "Success" });
});

server.post('/api/messages', connector.listen());

server.use(render({
    engine: {
        name: 'pug',
        extname: 'pug'
    },
    dir: __dirname
}))
// server.get('/docs/*', restify.plugins.serveStatic({
//     directory: './pages/',
//     default: 'AchieversRecognitionUI.html',
//     appendRequestPath: false    
// }));

server.get('/docs/*', restify.plugins.serveStatic({
    directory: './pages/',
    default: 'viewHandler.js',
    appendRequestPath: false
}));

server.get('/views/*', function (req, res, next) {
    res.render('view', { pgDetails: viewSettings });
})

// getNames: "/api/achievers/userFetch",
//     getRecogTypes = "/api/achievers/getRecogTypes",
//     postrecog = "/api/achievers/postRecog"
server.use(restify.plugins.queryParser());

server.use(restify.plugins.bodyParser());

server.get('/api/achievers/userFetch', function (req, res, next) {
    try {
        console.log("inside userFetch");
        // console.log(req);
        var uName = req.query.userName;
        let userId = req.query.userId;
        let tenantId = req.query.tenantId;
        let email = req.query.email;

        cb = function (response, err) {
            if (err) {
                console.log("Error in calling the get api");
                console.log(err);
                res.send(400, "Error in getting response from achievers API");
            }
            else {
                if (response.user.achieverAccessToken) {
                    if (response.user.achieverAccessToken.length > 0) {
                        const options = {
                            uri: achieverAPI.baseUrl + achieverAPI.getUsers,
                            qs: {
                                'q': uName
                            },
                            headers: {
                                'Authorization': 'Bearer ' + response.user.achieverAccessToken
                            },
                            json: true
                        };
                        request(options)
                            .then(function (response) {
                                console.log('inside request handler in userFetch')
                                console.log(email);
                                console.log(response);
                                if (response.items.length > 0) {
                                    for (i = 0; i < response.items.length; i++) {
                                        if (response.items[i].emailAddress == email) {
                                            response.items.splice(i, 1);
                                        }
                                    }
                                }
                                // console.log(response);
                                userData = JSON.stringify(response);
                                res.writeHead(200, {
                                    'Content-Length': Buffer.byteLength(userData),
                                    'Content-Type': 'application/json'
                                });
                                res.write(userData);
                                res.end();
                            })
                            .catch(function (err) {
                                console.log("Error in calling the get api");
                                console.log(err);
                                res.send(400, "Error in getting response from achievers API");
                            });
                    }
                }
            }
        }
        botAuth.getUserInfo(userId, tenantId, cb)
    }
    catch (e) {
        console.log("Exception in get user data for dropdown");
        console.log(e);
        res.send(400, "Error in getting response from achievers API");
    }
})

server.post('/api/achievers/postRecog', function (req, res, next) {
    console.log("inside Post Recognition");
    console.log(req);
    try {
        cb = function (response, err) {
            if (err) {
                console.log("There was an error creating your recognition. Please try again.");
                console.log(err);
                res.send(400, "There was an error creating your recognition. Please try again.");
            }
            else {
                // console.log(response.user.achieverAccessToken);
                if (response.user.achieverAccessToken) {
                    if (response.user.achieverAccessToken.length > 0) {
                        console.log("inside if condition");
                        const recogParams = {
                            uri: achieverAPI.baseUrl + achieverAPI.postRecog,
                            method: 'POST',
                            formData: {
                                nominees: req.body.nominees,
                                recognitionText: req.body.recognitionText,
                                criterionId: req.body.criterionId
                            },
                            headers: {
                                'Authorization': 'Bearer ' + response.user.achieverAccessToken,
                                'Content-Type': 'multipart/form-data; charset=UTF-8'
                            },
                            json: true
                        };
                        console.log(achieverAPI.baseUrl + achieverAPI.postRecog);
                        console.log("constructed params");

                        request(recogParams)
                            .then(function (resp) {
                                // console.log(resp);
                                var respRet = {
                                    message: "",
                                    newsFeedUrl: ""
                                }
                                console.log("inside submit post response handler");
                                console.log(resp);
                                var statusCode = 200;
                                if (resp.code && resp.code == -1) {
                                    if (resp.message.indexOf("Invalid point amount provided") > -1) {
                                        respRet.message = "You do not have enough points for this recognition. Consider sending one without points.";
                                    }
                                    else {
                                        respRet.message = resp.message;
                                    }
                                    statusCode = 400;
                                }
                                else {
                                    respRet.message = 'Success';
                                    respRet.newsFeedUrl = resp.newsfeedEventURL;
                                    statusCode = 200;
                                }
                                var finResp = JSON.stringify(respRet);
                                res.writeHead(statusCode, {
                                    'Content-Length': Buffer.byteLength(finResp),
                                    'Content-Type': 'application/json'
                                });
                                res.write(finResp);
                                res.end();
                            })
                            .catch(function (err) {
                                // console.log("Error in calling the post recognition api");
                                // console.log(err);
                                var respRet = {
                                    message: "",
                                    newsFeedUrl: ""
                                }

                                respRet.message = err.error.message;

                                if (respRet.message.indexOf("Invalid point amount provided") > -1) {
                                    respRet.message = "You do not have enough points for this recognition. Consider sending one without points.";
                                }
                                console.log(respRet.message);
                                var finResp = JSON.stringify(respRet);
                                res.writeHead(400, {
                                    'Content-Length': Buffer.byteLength(finResp),
                                    'Content-Type': 'application/json'
                                });
                                res.write(finResp);
                                res.end();
                            })
                    }
                }
            }
        }
        botAuth.getUserInfo(req.body.appUserId, req.body.tenantId, cb);
    }
    catch (e) {
        console.log("There was an error creating your recognition. Please try again.");
        console.log(e);
        res.send(400, "There was an error creating your recognition. Please try again.");
    }

})

server.post('/api/achievers/getRecogTypes', function (req, res, next) {
    try {
        cb = function (resp, err) {
            if (err) {
                console.log("Error in calling the getRecogTypes api");
                console.log(err);
                res.send(400, "Error in getting response from achievers API");
            }
            else {
                console.log("inside getRecogTypes");
                // console.log(achAuth.AchieversAuth.TokenVal);
                if (resp.user.achieverAccessToken) {
                    if (resp.user.achieverAccessToken.length > 0) {
                        const optionsRecog = {
                            uri: achieverAPI.baseUrl + achieverAPI.getRecogTypes,
                            method: 'OPTIONS',
                            qs: {
                                page: 1,
                                pageSize: 100,
                                useUserIds: true,
                                nominees: req.body.userIds.join()
                            },
                            headers: {
                                'Authorization': 'Bearer ' + resp.user.achieverAccessToken
                            },
                            json: true
                        };

                        request(optionsRecog)
                            .then(function (response) {
                                // console.log(response);
                                recogTypes = JSON.stringify(response);
                                res.writeHead(200, {
                                    'Content-Length': Buffer.byteLength(recogTypes),
                                    'Content-Type': 'application/json'
                                });
                                res.write(recogTypes);
                                res.end();
                            })
                            .catch(function (err) {
                                console.log("Error in calling the getRecogTypes api");
                                console.log(err);
                                res.send(400, "Error in getting response from achievers API");
                            });
                    }
                }
            }
        }

        botAuth.getUserInfo(req.body.appUserId, req.body.tenantId, cb);
    }
    catch (e) {
        console.log("Error in getRecogTypes: " + e);
        res.send(400, "Error in getting response from achievers API");
    }
})

const bot = new builder.UniversalBot(connector).set('storage', inMemoryStorage);
// // Do not persist userData
// bot.set(`persistUserData`, false);

// // Do not persist conversationData
// bot.set(`persistConversationData`, false);

const checkAuth = function () {
    if (botAuth.AchieversAuth.AccessCode.length > 0) {
        return true;
    }
    else {
        return false;
    }
}

bot.dialog('/', [
    function (session) {
        try {
            session.sendTyping();
            var userId = session.message.address.user.id;
            var tenantId = session.message.sourceEvent.tenant.id;
            console.log("inside bot.dialog");
            cb = function (resp, err) {
                if (err) {
                    console.log(`error in bot.dialog: ${err}`);
                    session.endDialog("Error communicating with our servers at the moment. Please try again after sometime...");
                }
                else {
                    if (resp.user && resp.user.achieverAccessToken) {
                        if (resp.user.achieverAccessToken.length > 0) {
                            var text = session.message.text;
                            var message = session.message;
                            var botMri = message.address.bot.id.toLowerCase();
                            console.log(botMri);
                            console.log(message.entities);
                            if (message.entities) {
                                message.entities
                                    .filter(entity => ((entity.type === "mention") && (entity.mentioned.id.toLowerCase() === botMri)))
                                    .forEach(entity => {
                                        text = text.replace(entity.text, "");
                                    });
                                text = text.trim();
                            }
                            console.log(text);
                            if (text.toLowerCase().trim() == "sign out") {
                                session.beginDialog('Sign Out');
                            }
                        }
                    }
                    else {
                        botAuth.BotSignIn(session);
                    }
                }
            }
            var text = session.message.text;
            var botMri = session.message.address.bot.id.toLowerCase();
            if (session.message.entities) {
                session.message.entities
                    .filter(entity => ((entity.type === "mention") && (entity.mentioned.id.toLowerCase() === botMri)))
                    .forEach(entity => {
                        text = text.replace(entity.text, "");
                    });
                text = text.trim();
            }
            if (text.toLowerCase().trim() == "help") {
                session.beginDialog('Help');
            }
            else if (text.toLowerCase().trim() == "sign in") {
                session.beginDialog('Sign In');
            }
            else if (text.toLowerCase().trim() == "sign out") {
                session.beginDialog('Sign Out');
            }
            else if (text.toLowerCase().trim() == "roster test") {
                session.beginDialog('Roster Test');
            }
            else {
                builder.Prompts.choice(session, "Choose an option:", 'Help', 'Sign In');
            }
        }
        catch (e) {
            console.log(`error in bot.dialog: ${e}`);
            session.endDialog("Error communicating with our servers at the moment. Please try again after sometime...");
        }
    },
    function (session, results) {
        switch (results.response.index) {
            case 0:
                session.beginDialog('Help');
                break;
            case 1:
                session.beginDialog('Sign In');
                break;
        }
    }
]);

bot.dialog('Help', function (session) {
    // session.endDialog("Help is on the way. Hang on... :P");
    session.sendTyping();
    try {
        var card = {
            "contentType": "application/vnd.microsoft.card.adaptive",
            "content": {
                "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                "type": "AdaptiveCard",
                "version": "1.0",
                "body": [
                    {
                        "type": "TextBlock",
                        "text": "Hi there, I'm Achievers",
                        "color": "Accent",
                        "size": "Large"
                    },
                    {
                        "type": "TextBlock",
                        "text": "A teammate of yours recently added me to \nhelp your team recognize colleagues right from Teams!",
                        "spacing": "Medium",
                        "wrap": true
                    },
                    {
                        "type": "Image",
                        "url": baseUrl + "/docs/help_image.png",
                        "id": "img_help",
                        "size": "Auto"
                    },
                    {
                        "type": "TextBlock",
                        "text": "To recognize a colleague in this channel, simply activate the Achievers Recognition creation via the (…) menu below the message input box.",
                        "wrap": true
                    }
                ],
                "actions": [
                    {
                        "type": "Action.OpenUrl",
                        "title": "Learn more",
                        "url": "https://www.achievers.com"
                    }
                ]
            }
        }


        var msg = new builder.Message(session)
            .addAttachment(card);
        session.send(msg);
        session.endDialog();
        // var msg = new builder.Message(session);
        // msg.attachmentLayout(builder.AttachmentLayout.carousel)
        // msg.attachments([
        // new builder.HeroCard(session)
        //     .title("Hi there, I'm Achievers")
        //     .subtitle("A teammate of yours recently added me to \nhelp your team recognize colleagues right from Teams!")
        //     .text("To recognize a colleague in this channel, simply activate the Achievers Recognition creation via the (…) menu below the message input box.")
        //     .images([builder.CardImage.create(session, baseUrl + "/docs/help_image.png")])
        // ]);
        // session.send(msg);
        // session.endDialog();
    }
    catch (e) {
        console.log(`error in help dialog handler: ${e}`);
        session.endDialog("Error while processing your request. Please try again after sometime.");
    }
});

bot.dialog('Sign Out', function (session) {
    // session.endDialog('This section is currently under development');
    try {
        session.sendTyping();
        var userId = session.message.address.user.id;
        var tenantId = session.message.sourceEvent.tenant.id;

        signOutCb = function (response, err) {
            if (err) {
                session.endDialog("There was trouble signing you out. Please try again after some time.");
            }
            else {
                console.log(response)
                session.endDialog("You have been successfully signed out. Please note that you have to sign in again to be able to utilize Achiever's app functionality.");
            }
        }
        cb = function (resp, err) {
            if (err) {
                session.endDialog("There was trouble signing you out. Please try again after some time.");
            }
            else {
                if (resp.user && resp.user.achieverAccessToken) {
                    if (resp.user.achieverAccessToken.length > 0) {
                        botAuth.BotSignOut(session, signOutCb);
                    }
                }
                else {
                    session.endDialog("You are already signed out.");
                }
            }
        }
        botAuth.getUserInfo(userId, tenantId, cb);
    }
    catch (e) {
        console.log(`error in sign out: ${e}`);
        session.endDialog("There was trouble signing you out. Please try again after some time.");
    }
})

bot.dialog('Sign In', function (session) {
    botAuth.BotSignIn(session);
})

bot.dialog('Recognize', function (session) {
    session.endDialog("This section is currently under development");
});

bot.dialog('Roster Test', function (session) {
    try {
        var teamId = session.message.sourceEvent.team.id;
        var mentions = [];

        cb = function () {
            console.log(mentions);
            var msg = new teams.TeamsMessage(session).text(teams.TeamsMessage.getTenantId(session.message));
            var mentionedmsg = "";
            if (mentions && mentions.length > 0) {
                for (i = 0; i < mentions.length; i++) {
                    mentionedmsg = msg.addMentionToText(mentions[i]);
                }
            }
            console.log(mentionedmsg);
            // var generalMessage = mentionedmsg.routeReplyToGeneralChannel();
            session.send(mentionedmsg);
            session.endDialog();
        }
        if (teamId && teamId.length > 0) {
            connector.fetchMembers(session.message.address.serviceUrl, teamId, function (err, result) {
                if (err) {
                    console.log('Fetch members error');
                    console.log(err);
                }
                else {
                    // console.log(result);
                    console.log("inside fetch members else");
                    console.log(result);
                    for (i = 0; i < result.length; i++) {
                        var mention = {
                            name: result[i].givenName + result[i].surname,
                            id: result[i].id
                        }
                        mentions.push(mention);
                    }
                    cb();
                }
            })
        }

    }
    catch (e) {
        console.log("error in roster test.");
        console.log(e);
        session.endDialog("Error in roster test.");
    }

});

bot.on('conversationUpdate', function (message) {
    // console.log(message);
    try {
        console.log(message);
        var members = message.membersAdded;

        var card = {
            "contentType": "application/vnd.microsoft.card.adaptive",
            "content": {
                "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                "type": "AdaptiveCard",
                "version": "1.0",
                "body": [
                    {
                        "type": "TextBlock",
                        "text": "Hi there, I'm Achievers",
                        "color": "Accent",
                        "size": "Large"
                    },
                    {
                        "type": "TextBlock",
                        "text": "A teammate of yours recently added me to \nhelp your team recognize colleagues right from Teams!",
                        "spacing": "Medium",
                        "wrap": true
                    },
                    {
                        "type": "Image",
                        "url": baseUrl + "/docs/help_image.png",
                        "id": "img_help",
                        "horizontalAlignment": "Center",
                        "size": "Large"
                    },
                    {
                        "type": "TextBlock",
                        "text": "To recognize a colleague in this channel, simply activate the Achievers Recognition creation via the (…) menu below the message input box.",
                        "wrap": true
                    }
                ],
                "actions": [
                    {
                        "type": "Action.OpenUrl",
                        "title": "Learn more",
                        "url": "https://www.achievers.com"
                    }
                ]
            }
        }

        for (var i = 0; i < members.length; i++) {

            // See if the member added was our bot
            if (members[i].id.includes(MicrosoftAppId)) {
                var botmessage = new builder.Message()
                    .address(message.address)
                    .addAttachment(card);

                bot.send(botmessage, function (err) { });
            }
        }
    }
    catch (e) {
        console.log(`error in conversationUpdate: ${e}`);
    }
});

const getMembers = function (ConversationId, serviceUrl, nomineeEmails) {
    var mentions = [];
    try {
        cb = function () {
            return mentions;
        }
        connector.fetchMembers(serviceUrl, conversationId, function (err, result) {
            if (err) {
                console.log('Fetch members error');
                console.log(err);
                cb();
            }
            else {
                // console.log(result);
                console.log("inside fetch members else");
                for (i = 0; i < result.length; i++) {
                    if (nomineeEmails.includes(result[i].email)) {
                        var mention = {
                            name: result[i].givenName + result[i].surname,
                            id: result[i].id
                        }
                    }
                }
                cb();
            }
        });
    }
    catch (e) {
        console.log("error in getMembers method");
        console.log(e);
        return mentions;
    }
};

const composeExtensionSubmitActionHandler = function (event, req, callback) {
    //Submit event handling comes here.
    try {
        // addConjunction = function (strVal) {
        //     strVal = strVal.replace(/,\s*$/, " and ")
        //     strVal = strVal.replace(",", ", ");
        //     return strVal;
        // }
        conversationId = event.address.conversation.id;
        serviceUrl = event.address.serviceUrl;
        // var mentions = this.getMembers(conversationId, serviceUrl, req.data.nomineeEmails);

        console.log("inside submit action handler");
        console.log(req);
        // console.log("event below:")
        // console.log(event);
        // console.log("request below:");
        // console.log(req);
        var adaptiveCard = {
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "text": "Recognized for " + req.data.criterionVal,
                    "size": "Large"
                },
                {
                    "type": "TextBlock",
                    "text": req.data.nominees,
                    "weight": "Bolder",
                    "spacing": "None"
                },
                {
                    "type": "TextBlock",
                    "text": req.data.recognitionText,
                    "spacing": "Medium"
                },
                {
                    "type": "TextBlock",
                    "text": "From: " + req.data.userFullName,
                    "spacing": "Medium"
                }
            ],
            "actions": [
                {
                    "type": "Action.OpenUrl",
                    "title": "View Recognition",
                    "url": req.data.newsFeedUrl
                }
            ],
            "version": "1.0"
        };
        var subResponse = {
            "composeExtension": {
                "type": "result",
                "attachmentLayout": "list",
                "preview": {
                    "contentType": "application/vnd.microsoft.card.adaptive",
                    "content": adaptiveCard
                },
                "attachments": [
                    {
                        "contentType": "application/vnd.microsoft.card.adaptive",
                        "content": adaptiveCard
                    }
                ]
            }
        }
        callback(null, subResponse, 200);
    }
    catch (e) {
        console.log("exception in form submit");
        console.log(e);
        callback(e, subResponse, 400);
    }
};



const composeExtensionFetchHandler = function (event, req, callback) {
    // console.log(req);  
    // console.log(event);
    try {
        let userId;
        let conversationId;
        let serviceUrl;
        let email;
        let tenantId;
        let name;

        userId = event.address.user.id;
        conversationId = event.address.conversation.id;
        serviceUrl = event.address.serviceUrl;
        tenantId = event.sourceEvent.tenant.id;
        name = event.address.user.name;
        // // console.log("ConversationId below");
        // // console.log(conversationId);
        // accessToken = botAuth.getUserInfo();

        cb = function (response) {
            console.log("at extensionFetchHandler after receiving response");
            // console.log(response);
            if (response.user && response.user.achieverAccessToken) {
                if (response.user.achieverAccessToken.length > 0) {
                    console.log("Inside OPenFOrm");

                    // if (achAuth.AchieversAuth.AccessCode.trim().length > 0) {
                    var response = new teams.TaskModuleContinueResponse()
                        .url(baseUrl + "/views/viewRecognizePage/?userId=" + userId + "&tenantId=" + tenantId + "&email=" + email + "&userName=" + name)
                        .height("large")
                        .width("large")
                        .title("Achievers Recognition portal");

                    // console.log("response object created");
                    // console.log(response.toResponseOfFetch());
                    callback(null, response.toResponseOfFetch(), 200);
                }
            }
            else {
                let resp = botAuth.ExtensionSignIn(event);
                callback(null, resp, 200);
            }
        };

        connector.fetchMembers(serviceUrl, conversationId, function (err, result) {
            if (err) {
                console.log('Fetch members error');
                console.log(err);
            }
            else {
                // console.log(result);
                console.log("inside fetch members else");
                for (i = 0; i < result.length; i++) {
                    if (userId == result[i].id) {
                        email = result[i].email;
                    }
                }
                console.log(email);
                botAuth.getUserInfo(userId, tenantId, cb);
            }
        });
    }
    catch (e) {
        console.log("Error inside extension fetch task handler.");
        console.log(e);
        callback(e, null, 400);
    }
};

connector.onComposeExtensionSubmitAction(composeExtensionSubmitActionHandler);

connector.onComposeExtensionFetchTask(composeExtensionFetchHandler);

// var composeExtensionHandler = function (event, query, callback) {
//     console.log("inside composeExtensionHandler");
//     if (achAuth.AchieversAuth.AccessCode.length > 0) {
//         // resp = {
//         //     "name": "composeExtension/submitAction",
//         //     "type": "invoke",
//         //     "value": {
//         //         "type": "task/fetch"
//         //     }
//         // };
//         console.log("inside auth complete if condition");
//         resp =
//             {
//                 "task": {
//                     "type": "continue",
//                     "value": {
//                         "card": {
//                             "contentType": "application/vnd.microsoft.card.adaptive",
//                             "content": {
//                                 "actions": [
//                                     {
//                                         "type": "invoke",
//                                         "title": "Recognize",
//                                         "value": {
//                                             "type": "task/fetch",
//                                         }
//                                     }
//                                 ],
//                                 "type": "AdaptiveCard",
//                                 "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
//                                 "version": "1.0"
//                             }
//                         }
//                     }
//                 }
//             };

//         callback(null, resp, 200);
//     }
//     else {
//         let resp = botAuth.ExtensionSignIn(event);
//         // console.log(resp);
//         callback(null, resp, 200);
//     }
// };
// connector.onQuery('sample', composeExtensionHandler);

connector.onSigninStateVerification(function (event, query, callback) {
    console.log("inside onSigninStateVerification");
    try {
        var userId = event.address.user.id;
        var tenantId = event.sourceEvent.tenant.id;

        cb = function (resp, err) {
            if (resp.user && resp.user.achieverAccessToken) {
                if (resp.user.achieverAccessToken.length > 0) {
                    var msg = new builder.Message().address(event.address)
                        .text("You have now successfully signed in");

                    connector.send([msg.toMessage()], function (err, address) {
                        console.log(err);
                    })
                }
                else {
                    var msg = new builder.Message().address(event.address)
                        .text("We are experiencing troubles with the sign in.");

                    connector.send([msg.toMessage()], function (err, address) {
                        console.log(err);
                    })
                }
            }
            else {
                var msg = new builder.Message().address(event.address)
                    .text("We are experiencing troubles with the sign in.");

                connector.send([msg.toMessage()], function (err, address) {
                    console.log(err);
                })
            }

            callback(null, null, 200);
            return;
        };
        botAuth.getUserInfo(userId, tenantId, cb);
    }
    catch (e) {
        var msg = new builder.Message().address(event.address)
            .text("We are experiencing troubles with the sign in.");

        connector.send([msg.toMessage()], function (err, address) {
            console.log(err);
        })
    }
});

// connector.onSigninStateVerification(function (event, query, callback) { return botAuth.verifySigninState(event, query, callback); });

// connector.onComposeExtensionSubmitAction
// bot.dialog('Signin', function (session) { return botAuth.botSignIn(session); });
// bot.dialog('Signout', function (session) { return botAuth.botSignOut(session); });
// connector.onSigninStateVerification(function (event, query, callback) { return botAuth.verifySigninState(event, query, callback); });