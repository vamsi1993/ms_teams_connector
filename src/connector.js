const BotConnector = require('botframework-connector');

const credentials = new BotConnector.MicrosoftAppCredentials({
    appId: process.env.appId,
    appPassword: process.env.appPassword
});

var client = new BotConnector.ConnectorClient(credentials);

var botId = '';
var recipientId = ''

client.conversations.createConversation({
    bot: { id: botId },
    members: [
        { id: recipientId }
    ],
    isGroup: false
}).then(result => {
    var conversationId = result.id;
    return client.conversations.sendToConversation(conversationId, {
        type: "message",
        from: { id: botId },
        recipient: { id: recipientId },
        text: 'This a message from Bot Connector Client (NodeJS)'
    });
}).then(result => {
    var activityId = result.id;
    console.log('Sent reply with ActivityId:', activityId);
});