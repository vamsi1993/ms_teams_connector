FROM node:10


WORKDIR /home/node/app

COPY package*.json /home/node/app/

RUN npm install
 

EXPOSE 4001
ENTRYPOINT ["npm","run","start"]
#CMD tail -f /dev/null
