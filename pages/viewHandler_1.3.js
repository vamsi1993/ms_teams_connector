var global = {};
$(document).ready(function () {
    microsoftTeams.initialize();
    global.userData = {
        nomineesSelected: [],
        recType: '',
        recVal: '',
        criterion: '',
        recogText: '',
    }

    let queryParams = getUrlVars();
    pgDetails.userId = queryParams["userId"];
    pgDetails.tenantId = queryParams["tenantId"];
    pgDetails.userEmail = queryParams["email"];
    pgDetails.userFullName = decodeURI(queryParams["userName"]);

    currentRecogData = null;
    currentPageStatus = 1;
    timeoutID = null;

    $("#userDetails").show();
    $("#recogDetails").hide();
    $("#previewRecog").hide();
    $("#recipientContainer").hide();
    // $("#selRtype").change( function(){
    //     clearAllWarnings();
    //     populateRecogValues();
    // });

    $("#selRtype").selectric({
        onChange: function(){
            clearAllWarnings();
            populateRecogValues();
        }
    });
    // $("#selRval").change(function () {
    //     clearAllWarnings();
    //     global.userData.recVal = $("#selRval").val();
    //     global.userData.criterion = $("#selRval  option:selected").text();
    // })
    $("#selRval").selectric({
        onChange: function(){
            clearAllWarnings();
            global.userData.recVal = $("#selRval").val();
            global.userData.criterion = $("#selRval  option:selected").text();
        }
    });

    $("#btn_Target2").click(function () {

        if (pageOneValid()) {
            $("#userDetails").hide();
            $("#recogDetails").show();
            $("#previewRecog").hide();
            global.currentPageStatus = 2;
        }
    });

    $("#inpRmessage").focus(clearAllWarnings());

    $("input[type=text]").focus(clearAllWarnings());
    $("select").focus(clearAllWarnings());
    // $(":input").click(function(){
    //     clearAllWarnings();
    // })
    // $(":button").click(function(){
    //     clearAllWarnings();
    // })

    $("#btn_Preview").click(function () {
        global.userData.recogText = $("#inpRmessage").val();
        if (global.userData.recogText.length > 0) {
            $("#userDetails").hide();
            $("#recogDetails").hide();
            //form preview
            $("#recogTypePlaceholder").text('"' + global.userData.criterion + '"');
            $("#previewRecogNominees").text(getCommaSeparatedString(getFullNames(global.userData.nomineesSelected)));
            $("#previewRecogMessage").text(global.userData.recogText);
            $("#previewRecogFrom").text(" " + pgDetails.userFullName);
            $("#previewRecog").show();
        }
        else {
            alertWarningRecog("You must enter a recognition message.");
        }
    })

    $("#btn_Preview_Back").click(function () {
        $("#userDetails").show();
        $("#recogDetails").hide();
        $("#previewRecog").hide();
    })

    $("#btn_Submit_Back").click(function () {
        $("#userDetails").hide();
        $("#recogDetails").show();
        $("#previewRecog").hide();
    })

    $("#btn_Submit").click(function () {
        postRecognition();
    })

    $("#to").keyup(function (e) {
        clearTimeout(global.timeoutID);
        global.timeoutID = setTimeout(() => getSearchUserData(e.target.value), 500);
    });

    $("#to").focus(clearAllWarnings());
})
//Search User Data
function getSearchUserData(str) {
    if (str.length == 0) {
        removeElement("autocomplete-list");
    }
    else {
        load(true);
        try {
            if (str.length == 0) {
                load(false);
                return;
            }
            $.ajax({
                url: pgDetails.baseAppUrl + pgDetails.getNames,
                data: {
                    userName: str,
                    userId: pgDetails.userId,
                    tenantId: pgDetails.tenantId,
                    email: pgDetails.userEmail
                },
                type: 'GET',
                crossDomain: true,
                success: function (data) {
                    load(false);
                    var a, b, i
                    removeElement("autocomplete-list");
                    a = document.createElement("div");
                    a.setAttribute("id", "autocomplete-list");
                    a.setAttribute("class", "autocomplete-items container-fluid");
                    $("#grpUserNamSel").append(a);
                    if(data.items.length>0)
                    {
                        $("#recipientContainer").show();
                    }
                    $.each(data.items, function (i, item) {
                        b = document.createElement("div");
                        b.setAttribute("class", "row");
                        b.innerHTML = '<image class="rounded-circle searchImg" src="' + item.profileImageUrl + '" onerror="this.src=\'' + pgDetails.baseAppUrl + '/docs/icon_med.jpg' +'\';"><p class="searchName">' + item.fullName + '</p>';
                        // b.innerHTML = "<p id=" + item.id + ">" + item.fullName + "</p>";
                        if (!checkUserPresence(global.userData.nomineesSelected, item.id)) {
                            b.addEventListener("click", function (e) {
                                try {
                                    $("#to").val('');
                                    var container = $("#recipientsArea");
                                    // var el = '<div id="' + item.id + '" class="row userDisplayPanel"><div class="col-sm-2"><image src="' + item.profileImageUrl + '" onerror="this.src=\'' + pgDetails.baseAppUrl + '/docs/icon_med.jpg' +'\';" class="rounded-circle recipientImage"></div><div class="col-sm-8"> <span class=receipientName> ' + item.fullName + ' </span> </div><div class="col-sm-2 closeText"><a class="icon close" onclick="deletePanel(this)"></a></div></div>'
                                    var el = '<div id="' + item.id + '" class="row userDisplayPanel"><image src="' + item.profileImageUrl + '" onerror="this.src=\'' + pgDetails.baseAppUrl + '/docs/icon_med.jpg' +'\';" class="rounded-circle recipientImage"><span class=receipientName> ' + item.fullName + ' </span><a class="icon close" onclick="deletePanel(this)"></a></div>'
                                    // var element = "<li class='list-group-item' id="+ item.id +">" + item.fullName + "</li>";
                                    container.append(el);
                                    // if(!jQuery.inArray(item.id, global.userData.nomineesSelected))
                                    // {
                                    var userItem = { userId: item.id, userFullName: item.fullName, userName: item.username, userEmail: item.emailAddress }
                                    global.userData.nomineesSelected.push(userItem);
                                    // }
                                    closeAllLists();
                                    recognitionTypes();

                                }
                                catch (e) {
                                }
                            },{passive:false})
                            a.append(b);
                        }
                    });
                },
                error: function (data) {
                    load(false);
                }
            });
        }
        catch (e) {
            load(false);
        }
    }
}

function closeAllLists() {
    $("#autocomplete-list").remove();
}

function deletePanel(el) {
    load(true);
    var divel = $(el).parent();
    var uId = divel.attr('id');
    $(divel).remove();
    var nom = global.userData.nomineesSelected;
    for (i = 0; i < nom.length; i++) {
        if (nom[i].userId == uId) {
            nom.splice(i, 1);
        }
    }
    if (global.userData.nomineesSelected.length > 0) {
        recognitionTypes();
    }
    else {
        clearRecogTypes();
    }
    load(false);
}


function recognitionTypes() {
    load(true);
    try {
        clearRecogTypes();
        var _userIds = getUserIds(global.userData.nomineesSelected);
        $.ajax({
            url: pgDetails.baseAppUrl + pgDetails.getRecogTypes,
            data: {
                userIds: _userIds,
                appUserId: pgDetails.userId,
                tenantId: pgDetails.tenantId
            },
            type: 'POST',
            crossDomain: true,
            success: function (data) {
                load(false);
                global.currentRecogData = data;
                $("#selRtype").append("<option value= '0' selected=true> select</option>");
                var option = '';
                for (i = 0; i < data.items.length; i++) {
                    option = "<option value= '" + data.items[i].id + "'>" + data.items[i].name + "</option>"
                    $("#selRtype").append(option);
                }
                $("#selRtype").selectric('refresh');
                $('#selRtype').selectric('init');
            },
            error: function (data) {
                load(false);
            }
        });
    }
    catch (e) {
        console.log(e);
        load(false);
    }
}

function postRecognition() {
    load(true);
    try {
        var reqData = {
            nominees: getUserNames(global.userData.nomineesSelected).join(),
            recognitionText: global.userData.recogText,
            criterionId: parseInt(global.userData.recVal),
            criterionVal: global.userData.criterion,
            appUserId: pgDetails.userId,
            tenantId: pgDetails.tenantId,
            userFullName: pgDetails.userFullName,
            newsFeedUrl:"",
            nomineeEmails: getNomineeEmails(global.userData.nomineesSelected)
        }
        // console.log(reqData);

        $.ajax({
            url: pgDetails.baseAppUrl + pgDetails.postrecog,
            data: reqData,
            type: 'POST',
            crossDomain: true,
            success: function (data, status, xhr) {
                console.log(data);
                // alertSuccess(data.message);
                if (status && status == 400) {
                    alertWarningSubmit(data.message);
                }
                else {
                    load(false);
                    appIds = [pgDetails.appId];
                    reqData.nominees = getCommaSeparatedString(getFullNames(global.userData.nomineesSelected));
                    reqData.newsFeedUrl = data.newsFeedUrl;
                    microsoftTeams.tasks.submitTask(reqData, appIds);
                }
            },
            error: function (resp, textStatus, errorMessage) {
                load(false);
                if (resp.responseJSON.message == 'Success') {
                    appIds = [pgDetails.appId];
                    microsoftTeams.tasks.submitTask(reqData, appIds);
                }
                alertWarningSubmit(resp.responseJSON.message);
            }
        });
    }
    catch (e) {
        console.log(e);
        load(false);
    }
}

function populateRecogValues() {
    load(true);
    global.userData.recType = $("#selRtype").val();
    clearRecogValues();
    var curVal = $("#selRtype").val();
    var mainCriteria = [];
    for (i = 0; i < global.currentRecogData.items.length; i++) {
        if (global.currentRecogData.items[i].id == curVal) {
            mainCriteria = global.currentRecogData.items[i].criteria;
        }
    }
    $("#selRval").append("<option value= '0' selected=true> select</option>")
    if (mainCriteria.length > 0) {
        for (i = 0; i < mainCriteria.length; i++) {
            var option = "<option value='" + mainCriteria[i].id + "'>" + mainCriteria[i].name + "</option>";
            $("#selRval").append(option);
        }
        $("#selRval").selectric('refresh');
        $('#selRval').selectric('init');
    }
    load(false);
}

function clearRecogTypes() {
    $("#selRtype").empty();
    global.userData.recType = '';
    $("#selRtype").selectric('refresh');
    $('#selRtype').selectric('init');
    clearRecogValues();

}
function clearRecogValues() {
    $("#selRval").empty();
    global.userData.recVal = '';
    global.userData.criterion = '';
    $("#selRval").selectric('refresh');
    $('#selRval').selectric('init');
}

function alertWarning(msg) {
    $("#errorMessage").html(msg);
}

function pageOneValid() {
    if (global.userData.nomineesSelected.length == 0) {
        alertWarning("You must select at least one recipient.");
        return false;
    }
    if (!global.userData.recType) {
        alertWarning("A <b>Recognition type</b> is required.");
        return false;
    }
    else if (global.userData.recType == '0' | global.userData.recType == 0) {
        alertWarning("A <b>Recognition type</b> is required.");
        return false;
    }
    if (!global.userData.recVal) {
        alertWarning("A <b>Recognition value</b> is required.");
        return false;
    }
    else if (global.userData.recVal == '0' | global.userData.recVal == 0) {
        alertWarning("A <b>Recognition value</b> is required.");
        return false;
    }
    return true;
}

function checkUserPresence(arr, val) {
    for (i = 0; i < arr.length; i++) {
        if (arr[i].userId == val) {
            return true;
        }
    }
    return false;
}

function getUserIds(arr) {
    var resArr = [];
    for (i = 0; i < arr.length; i++) {
        resArr.push(arr[i].userId);
    }
    return resArr;
}

function getUserNames(arr) {
    var resArr = [];
    for (i = 0; i < arr.length; i++) {
        resArr.push(arr[i].userName);
    }
    return resArr;
}

function getFullNames(arr) {
    var resArr = [];
    for (i = 0; i < arr.length; i++) {
        resArr.push(arr[i].userFullName);
    }
    return resArr;
}

function getNomineeEmails(arr){
    var resArr = [];
    for (i = 0; i < arr.length; i++) {
        resArr.push(arr[i].userEmail);
    }
    return resArr;
}

function alertSuccess(msg) {
    $("#successAlertMsg").text(msg);
    $("#successAlert").slideDown().delay(6000).slideUp();
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function cleanImageUri(uri) {
    var clean_uri = ''
    if (uri.indexOf("?") > 0) {
        clean_uri = uri.substring(0, uri.indexOf("?"));
    }
    return clean_uri;
}

function getCommaSeparatedString(arr) {
    var retString = '';
    if (arr.length > 0) {
        for (i = 0; i < arr.length; i++) {
            if(i== 0)
            {
                retString = arr[i];
            }
            else if (i == arr.length - 1) {
                retString = retString + ' and ' + arr[i];
            }
            else if(i>0){
                retString = retString + ', ' + arr[i];
            }

        }
    }
    return retString;
}


function load(changeSwitch) {
    if (changeSwitch) {
        $("#loader").show();
    }
    else {
        $("#loader").hide();
    }
}

function removeElement(id) {
    if ($('#' + id).length) {
        $('#' + id).remove();
    }
}

function alertWarningSubmit(msg) {
    $("#errorMessageSubmit").html(msg);
}

function alertWarningRecog(msg) {
    $("#errorMessageRecog").html(msg);
}

function clearAllWarnings() {
    $("#errorMessageSubmit").text("");
    $("#errorMessageRecog").text("");
    $("#errorMessage").text("");
}
